# Ionium
## What is it ?

Ionium is P2P system based on LoRa for radio communication and Bluetooth to communicate with smartphone.

### How to use it ?

Simply connect the hardware according to the included scheme and upload the code to Arduino. (Scheme not uploaded yet, Use DPS design as reference for now)

### You will need:
- Arduino MEGA (Uno or lower will work but you cannot upload code while having Bluetooth powered)
- Bluetooth module (HC05 or higher)
- LoRa Module

### Libraries used:
- [sandeepmistry LoRa Library](https://github.com/sandeepmistry/arduino-LoRa)
